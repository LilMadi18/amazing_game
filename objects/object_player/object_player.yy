{
    "id": "499ead61-3343-4269-9f70-619d8390b5a0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_player",
    "eventList": [
        {
            "id": "a2d96df6-c930-4ea3-93c6-00e93a53bcca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "499ead61-3343-4269-9f70-619d8390b5a0"
        },
        {
            "id": "de4cf2aa-f9d8-489e-a899-d96163a7949a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 5,
            "m_owner": "499ead61-3343-4269-9f70-619d8390b5a0"
        },
        {
            "id": "67c07fc3-299d-4732-937f-fa30a2cf68ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 37,
            "eventtype": 5,
            "m_owner": "499ead61-3343-4269-9f70-619d8390b5a0"
        },
        {
            "id": "01cd386d-6662-4338-86ec-4c72d40e88f6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 39,
            "eventtype": 5,
            "m_owner": "499ead61-3343-4269-9f70-619d8390b5a0"
        },
        {
            "id": "556a4dcb-a171-4a8c-a933-04edda4fc049",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 38,
            "eventtype": 5,
            "m_owner": "499ead61-3343-4269-9f70-619d8390b5a0"
        },
        {
            "id": "efb4f2e7-69fa-46c9-b024-4789dca4663a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 5,
            "m_owner": "499ead61-3343-4269-9f70-619d8390b5a0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "69e79f50-583b-4de2-857a-ebd8ddfa37d4",
    "visible": true
}