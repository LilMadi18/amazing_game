/// @DnDAction : YoYo Games.Movement.Set_Direction_Fixed
/// @DnDVersion : 1.1
/// @DnDHash : 3EAEDED6
/// @DnDArgument : "direction" "180"
direction = 180;

/// @DnDAction : YoYo Games.Instances.Sprite_Animation_Speed
/// @DnDVersion : 1
/// @DnDHash : 4C2A13D6
image_speed = 1;

/// @DnDAction : YoYo Games.Movement.Set_Speed
/// @DnDVersion : 1
/// @DnDHash : 229F7968
/// @DnDArgument : "speed" "4"
speed = 4;

/// @DnDAction : YoYo Games.Instances.Sprite_Scale
/// @DnDVersion : 1
/// @DnDHash : 2F73323B
image_xscale = 1;
image_yscale = 1;

/// @DnDAction : YoYo Games.Instances.Set_Sprite
/// @DnDVersion : 1
/// @DnDHash : 7564FEA3
/// @DnDArgument : "imageind" "image_index"
/// @DnDArgument : "spriteind" "sprite_player_left"
/// @DnDSaveInfo : "spriteind" "6f5446b1-e870-4bda-bf93-ae3545bc5bf4"
sprite_index = sprite_player_left;
image_index = image_index;