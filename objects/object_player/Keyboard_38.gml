/// @DnDAction : YoYo Games.Movement.Set_Direction_Fixed
/// @DnDVersion : 1.1
/// @DnDHash : 09008E3B
/// @DnDArgument : "direction" "90"
direction = 90;

/// @DnDAction : YoYo Games.Instances.Sprite_Scale
/// @DnDVersion : 1
/// @DnDHash : 20A75F0F
image_xscale = 1;
image_yscale = 1;

/// @DnDAction : YoYo Games.Movement.Set_Speed
/// @DnDVersion : 1
/// @DnDHash : 6FBA60F5
/// @DnDArgument : "speed" "4"
speed = 4;

/// @DnDAction : YoYo Games.Instances.Sprite_Animation_Speed
/// @DnDVersion : 1
/// @DnDHash : 2866ACC3
image_speed = 1;

/// @DnDAction : YoYo Games.Instances.Set_Sprite
/// @DnDVersion : 1
/// @DnDHash : 34BAE752
/// @DnDArgument : "imageind" "image_index"
/// @DnDArgument : "spriteind" "sprite_player_up"
/// @DnDSaveInfo : "spriteind" "15422c08-4d2e-48ca-b0f6-d1a2644fc815"
sprite_index = sprite_player_up;
image_index = image_index;