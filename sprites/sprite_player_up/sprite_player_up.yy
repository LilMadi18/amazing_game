{
    "id": "15422c08-4d2e-48ca-b0f6-d1a2644fc815",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4db55a32-543d-48ce-89e3-f9ba5bec56de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15422c08-4d2e-48ca-b0f6-d1a2644fc815",
            "compositeImage": {
                "id": "813edec1-365e-424f-82f7-3f6e560dca9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4db55a32-543d-48ce-89e3-f9ba5bec56de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ad2917b-700a-4d67-be85-a8fc9894913b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4db55a32-543d-48ce-89e3-f9ba5bec56de",
                    "LayerId": "4be3bb41-f03e-4abf-9872-644a43b47c61"
                }
            ]
        },
        {
            "id": "467ead0e-93d0-4535-a517-11eb8c30f02f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15422c08-4d2e-48ca-b0f6-d1a2644fc815",
            "compositeImage": {
                "id": "200a9a92-8c06-49e9-9db6-c4ca987a6c7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "467ead0e-93d0-4535-a517-11eb8c30f02f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78d43f38-72dc-4625-8831-d9f883bd479d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "467ead0e-93d0-4535-a517-11eb8c30f02f",
                    "LayerId": "4be3bb41-f03e-4abf-9872-644a43b47c61"
                }
            ]
        },
        {
            "id": "750ae2c3-d97b-4b18-86bd-2108d64641e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15422c08-4d2e-48ca-b0f6-d1a2644fc815",
            "compositeImage": {
                "id": "b2ee6c16-42fa-4c9c-bc83-439ebb0839ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "750ae2c3-d97b-4b18-86bd-2108d64641e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5fa3e85-2a81-4f3a-ba36-7dee47f744c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "750ae2c3-d97b-4b18-86bd-2108d64641e2",
                    "LayerId": "4be3bb41-f03e-4abf-9872-644a43b47c61"
                }
            ]
        },
        {
            "id": "a39e6655-523f-4da7-bfb4-0150ce19370d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15422c08-4d2e-48ca-b0f6-d1a2644fc815",
            "compositeImage": {
                "id": "6ebd3071-73c2-4b55-ad85-7af5daea9b5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a39e6655-523f-4da7-bfb4-0150ce19370d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a3ca17e-d2a5-4e70-8fe6-1226f89bba1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a39e6655-523f-4da7-bfb4-0150ce19370d",
                    "LayerId": "4be3bb41-f03e-4abf-9872-644a43b47c61"
                }
            ]
        },
        {
            "id": "a3141c2d-90b6-419a-996c-ea39b82aa314",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15422c08-4d2e-48ca-b0f6-d1a2644fc815",
            "compositeImage": {
                "id": "17abd852-c515-417a-aafc-972ad64d7c98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3141c2d-90b6-419a-996c-ea39b82aa314",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53df49e5-04b9-4699-9c3a-afcfb13aae8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3141c2d-90b6-419a-996c-ea39b82aa314",
                    "LayerId": "4be3bb41-f03e-4abf-9872-644a43b47c61"
                }
            ]
        },
        {
            "id": "e77bc248-da1a-42cc-b204-ce55d3b63057",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15422c08-4d2e-48ca-b0f6-d1a2644fc815",
            "compositeImage": {
                "id": "2b700518-76a0-459d-9749-94d06e8fd4cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e77bc248-da1a-42cc-b204-ce55d3b63057",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70dc8010-f354-4bdd-9049-2ffb72bd3579",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e77bc248-da1a-42cc-b204-ce55d3b63057",
                    "LayerId": "4be3bb41-f03e-4abf-9872-644a43b47c61"
                }
            ]
        },
        {
            "id": "1628780e-bf7c-41e2-b48e-4224e1990f43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15422c08-4d2e-48ca-b0f6-d1a2644fc815",
            "compositeImage": {
                "id": "403cf770-3b28-46b4-b21f-4b74182d136a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1628780e-bf7c-41e2-b48e-4224e1990f43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6c68b71-3cca-433a-bb74-03178c236f3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1628780e-bf7c-41e2-b48e-4224e1990f43",
                    "LayerId": "4be3bb41-f03e-4abf-9872-644a43b47c61"
                }
            ]
        },
        {
            "id": "2f3fbbcb-f68d-40f5-a543-9f3a28edc60d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15422c08-4d2e-48ca-b0f6-d1a2644fc815",
            "compositeImage": {
                "id": "f1def542-4110-411f-8a0f-dab3fc7daeb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f3fbbcb-f68d-40f5-a543-9f3a28edc60d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ed8474f-070f-4d2c-ac2f-069cf98f44f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f3fbbcb-f68d-40f5-a543-9f3a28edc60d",
                    "LayerId": "4be3bb41-f03e-4abf-9872-644a43b47c61"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4be3bb41-f03e-4abf-9872-644a43b47c61",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "15422c08-4d2e-48ca-b0f6-d1a2644fc815",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 0
}