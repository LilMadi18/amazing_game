{
    "id": "69e79f50-583b-4de2-857a-ebd8ddfa37d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7a318a15-f14c-4a31-9928-c26dcb308d47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69e79f50-583b-4de2-857a-ebd8ddfa37d4",
            "compositeImage": {
                "id": "d38a8594-5310-46ab-9e13-9f653ab52cf6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a318a15-f14c-4a31-9928-c26dcb308d47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eab9db40-376e-4797-9e3f-1a356035e449",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a318a15-f14c-4a31-9928-c26dcb308d47",
                    "LayerId": "b2306f45-7e97-42ef-a755-13615c8d852c"
                }
            ]
        },
        {
            "id": "ecf5585f-8843-4c36-9d5e-b49fd35ffb1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69e79f50-583b-4de2-857a-ebd8ddfa37d4",
            "compositeImage": {
                "id": "a0e77c9b-4ba6-43cc-88ea-f36638fd0e0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecf5585f-8843-4c36-9d5e-b49fd35ffb1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c0e98a9-442d-4625-af8c-a6d6ff09dbb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecf5585f-8843-4c36-9d5e-b49fd35ffb1d",
                    "LayerId": "b2306f45-7e97-42ef-a755-13615c8d852c"
                }
            ]
        },
        {
            "id": "7c0720e4-4122-4377-a701-aa7316a1233a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69e79f50-583b-4de2-857a-ebd8ddfa37d4",
            "compositeImage": {
                "id": "d79a7dd4-f871-4f04-9850-bd3f2e1c1160",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c0720e4-4122-4377-a701-aa7316a1233a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "964df85d-5bcb-4933-bade-f795368758b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c0720e4-4122-4377-a701-aa7316a1233a",
                    "LayerId": "b2306f45-7e97-42ef-a755-13615c8d852c"
                }
            ]
        },
        {
            "id": "3dfafffd-052b-4456-a494-14371fce5882",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69e79f50-583b-4de2-857a-ebd8ddfa37d4",
            "compositeImage": {
                "id": "5460bf7f-7e21-4dc0-baae-7cc11e551cf0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3dfafffd-052b-4456-a494-14371fce5882",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fc8f130-c237-4513-a04b-6aa6889571e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dfafffd-052b-4456-a494-14371fce5882",
                    "LayerId": "b2306f45-7e97-42ef-a755-13615c8d852c"
                }
            ]
        },
        {
            "id": "dea2416b-47af-4b8e-973f-324157d96381",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69e79f50-583b-4de2-857a-ebd8ddfa37d4",
            "compositeImage": {
                "id": "1fe3a968-cb3d-443c-8cab-c36942166901",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dea2416b-47af-4b8e-973f-324157d96381",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08062a16-0615-44fc-87b8-b5594c27a267",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dea2416b-47af-4b8e-973f-324157d96381",
                    "LayerId": "b2306f45-7e97-42ef-a755-13615c8d852c"
                }
            ]
        },
        {
            "id": "7d2e0998-1b33-4d01-957b-51f1b2f5d03e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69e79f50-583b-4de2-857a-ebd8ddfa37d4",
            "compositeImage": {
                "id": "2012f0c3-775d-4279-b07d-333413154980",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d2e0998-1b33-4d01-957b-51f1b2f5d03e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "865e69fc-c223-48b3-8927-daaf982781d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d2e0998-1b33-4d01-957b-51f1b2f5d03e",
                    "LayerId": "b2306f45-7e97-42ef-a755-13615c8d852c"
                }
            ]
        },
        {
            "id": "725fa6dc-b2f8-4d0d-a840-3d66464b7293",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69e79f50-583b-4de2-857a-ebd8ddfa37d4",
            "compositeImage": {
                "id": "4428058e-fd7f-41d8-8d64-dfa27e530030",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "725fa6dc-b2f8-4d0d-a840-3d66464b7293",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5c1ee94-1038-4551-9a84-eda31fa1a35a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "725fa6dc-b2f8-4d0d-a840-3d66464b7293",
                    "LayerId": "b2306f45-7e97-42ef-a755-13615c8d852c"
                }
            ]
        },
        {
            "id": "e75d287b-5f48-453f-a126-3435efaa3a2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69e79f50-583b-4de2-857a-ebd8ddfa37d4",
            "compositeImage": {
                "id": "ee04963e-e95a-41a7-90a3-615459153c80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e75d287b-5f48-453f-a126-3435efaa3a2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "342055ae-73ca-4fff-89ce-d0729fdc0e26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e75d287b-5f48-453f-a126-3435efaa3a2b",
                    "LayerId": "b2306f45-7e97-42ef-a755-13615c8d852c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b2306f45-7e97-42ef-a755-13615c8d852c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "69e79f50-583b-4de2-857a-ebd8ddfa37d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 0
}