{
    "id": "162de714-fbfb-4749-ae5d-a03cc93e3d03",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8c855a1d-dd25-49c8-8313-6203f2026f68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "162de714-fbfb-4749-ae5d-a03cc93e3d03",
            "compositeImage": {
                "id": "96fe286d-0097-4cca-a090-dc84f705a364",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c855a1d-dd25-49c8-8313-6203f2026f68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f6c6edd-b132-41b0-9344-4e8f309f18eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c855a1d-dd25-49c8-8313-6203f2026f68",
                    "LayerId": "defde259-2dde-4c54-8887-8599700e6450"
                }
            ]
        },
        {
            "id": "3d0cba65-4c70-4d83-a534-7c0d8f899330",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "162de714-fbfb-4749-ae5d-a03cc93e3d03",
            "compositeImage": {
                "id": "a6c0f882-650e-47e9-bb81-50df3eb73448",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d0cba65-4c70-4d83-a534-7c0d8f899330",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "645602c3-d3be-4df2-bfa8-592d23998537",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d0cba65-4c70-4d83-a534-7c0d8f899330",
                    "LayerId": "defde259-2dde-4c54-8887-8599700e6450"
                }
            ]
        },
        {
            "id": "4f0cac42-9b56-42e9-bfcd-88024e49e5bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "162de714-fbfb-4749-ae5d-a03cc93e3d03",
            "compositeImage": {
                "id": "3ba7c4ef-30e8-446d-9091-09c3d68ca703",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f0cac42-9b56-42e9-bfcd-88024e49e5bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81370e0d-5e7b-4a16-8157-602885b7713b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f0cac42-9b56-42e9-bfcd-88024e49e5bd",
                    "LayerId": "defde259-2dde-4c54-8887-8599700e6450"
                }
            ]
        },
        {
            "id": "a48dd394-c514-4b0f-8a45-094d43a0b8fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "162de714-fbfb-4749-ae5d-a03cc93e3d03",
            "compositeImage": {
                "id": "b4524d41-753e-4c61-b37e-52a6467efe05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a48dd394-c514-4b0f-8a45-094d43a0b8fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f329a0b1-a385-46db-9b01-bd06b1ba4f38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a48dd394-c514-4b0f-8a45-094d43a0b8fd",
                    "LayerId": "defde259-2dde-4c54-8887-8599700e6450"
                }
            ]
        },
        {
            "id": "d8678988-24c8-4bb6-b6ed-ccd83f321a23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "162de714-fbfb-4749-ae5d-a03cc93e3d03",
            "compositeImage": {
                "id": "d55788f2-a3e4-43ba-8a50-6b91b4556eb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8678988-24c8-4bb6-b6ed-ccd83f321a23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa2d4763-0e2b-4674-ab6c-216aa402b0fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8678988-24c8-4bb6-b6ed-ccd83f321a23",
                    "LayerId": "defde259-2dde-4c54-8887-8599700e6450"
                }
            ]
        },
        {
            "id": "fc670c50-6868-445a-9dd6-c346878190ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "162de714-fbfb-4749-ae5d-a03cc93e3d03",
            "compositeImage": {
                "id": "ca872275-48c5-4404-b9de-67ffa9c1f657",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc670c50-6868-445a-9dd6-c346878190ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "053b6772-a0c9-484b-b54f-1bdaca42af79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc670c50-6868-445a-9dd6-c346878190ae",
                    "LayerId": "defde259-2dde-4c54-8887-8599700e6450"
                }
            ]
        },
        {
            "id": "5bef1ba8-2a7e-42f9-a9de-2ea7e0ade361",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "162de714-fbfb-4749-ae5d-a03cc93e3d03",
            "compositeImage": {
                "id": "0bdc6133-f379-43cc-9ebb-c3ee686a7947",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bef1ba8-2a7e-42f9-a9de-2ea7e0ade361",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "033fc0bc-ca6d-4938-9452-ba058a0f5bd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bef1ba8-2a7e-42f9-a9de-2ea7e0ade361",
                    "LayerId": "defde259-2dde-4c54-8887-8599700e6450"
                }
            ]
        },
        {
            "id": "0a2aebdf-7c16-4632-ad88-1e93b2af74f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "162de714-fbfb-4749-ae5d-a03cc93e3d03",
            "compositeImage": {
                "id": "4c998163-9a5b-422e-aa2e-dc8d70e68ce3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a2aebdf-7c16-4632-ad88-1e93b2af74f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f792ae31-a27f-468e-9682-e3cfbedeef25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a2aebdf-7c16-4632-ad88-1e93b2af74f4",
                    "LayerId": "defde259-2dde-4c54-8887-8599700e6450"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "defde259-2dde-4c54-8887-8599700e6450",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "162de714-fbfb-4749-ae5d-a03cc93e3d03",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 0
}