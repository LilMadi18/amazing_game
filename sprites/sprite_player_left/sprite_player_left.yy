{
    "id": "6f5446b1-e870-4bda-bf93-ae3545bc5bf4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1c662f3f-3987-443b-b17a-b32f0cc9a38f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f5446b1-e870-4bda-bf93-ae3545bc5bf4",
            "compositeImage": {
                "id": "a2629a98-2f28-451e-8566-c39ac48486d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c662f3f-3987-443b-b17a-b32f0cc9a38f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "884ab2fc-f550-4650-9dcf-ab907975f004",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c662f3f-3987-443b-b17a-b32f0cc9a38f",
                    "LayerId": "f6be1f5a-0057-493d-8980-49ac6db512b5"
                }
            ]
        },
        {
            "id": "0bbc9604-1f34-4e42-a708-4a194f338993",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f5446b1-e870-4bda-bf93-ae3545bc5bf4",
            "compositeImage": {
                "id": "640a9f98-7517-451a-a872-285b36e634ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bbc9604-1f34-4e42-a708-4a194f338993",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a88e164-1a83-4452-9721-f9a953afa991",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bbc9604-1f34-4e42-a708-4a194f338993",
                    "LayerId": "f6be1f5a-0057-493d-8980-49ac6db512b5"
                }
            ]
        },
        {
            "id": "26bfa895-eb71-4e2a-bfb7-aed0be796023",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f5446b1-e870-4bda-bf93-ae3545bc5bf4",
            "compositeImage": {
                "id": "04923d57-f7de-409b-be16-35d69bd0ef02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26bfa895-eb71-4e2a-bfb7-aed0be796023",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06fba4d7-51e2-4e62-9c31-47170408d7a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26bfa895-eb71-4e2a-bfb7-aed0be796023",
                    "LayerId": "f6be1f5a-0057-493d-8980-49ac6db512b5"
                }
            ]
        },
        {
            "id": "9deb8841-bdea-4bc2-abeb-d99790f86fcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f5446b1-e870-4bda-bf93-ae3545bc5bf4",
            "compositeImage": {
                "id": "9a4300f3-49df-4a65-9f42-7f8b9cf1bf32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9deb8841-bdea-4bc2-abeb-d99790f86fcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc6a5bc7-73f6-4721-b073-54045e97812d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9deb8841-bdea-4bc2-abeb-d99790f86fcd",
                    "LayerId": "f6be1f5a-0057-493d-8980-49ac6db512b5"
                }
            ]
        },
        {
            "id": "847de4c1-a583-44e1-b7c6-abafa649a9e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f5446b1-e870-4bda-bf93-ae3545bc5bf4",
            "compositeImage": {
                "id": "373bead8-e0df-42fb-9305-aa44d5e5bf02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "847de4c1-a583-44e1-b7c6-abafa649a9e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6d8ecdb-20d6-498b-acd4-45584fb26bfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "847de4c1-a583-44e1-b7c6-abafa649a9e7",
                    "LayerId": "f6be1f5a-0057-493d-8980-49ac6db512b5"
                }
            ]
        },
        {
            "id": "1baac42a-f9a0-4a10-81c7-0de0bc559f61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f5446b1-e870-4bda-bf93-ae3545bc5bf4",
            "compositeImage": {
                "id": "39e58e2d-c6ac-43ed-b6e1-25dc3dce1284",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1baac42a-f9a0-4a10-81c7-0de0bc559f61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c96bc05-dd17-4f4a-8a14-d543a11b4f94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1baac42a-f9a0-4a10-81c7-0de0bc559f61",
                    "LayerId": "f6be1f5a-0057-493d-8980-49ac6db512b5"
                }
            ]
        },
        {
            "id": "b706aba9-8975-4dc8-9fca-104c5072dea0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f5446b1-e870-4bda-bf93-ae3545bc5bf4",
            "compositeImage": {
                "id": "0d379474-da29-458a-8409-ddf98b13e273",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b706aba9-8975-4dc8-9fca-104c5072dea0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e2f4544-1cb6-4818-acf5-e29648008804",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b706aba9-8975-4dc8-9fca-104c5072dea0",
                    "LayerId": "f6be1f5a-0057-493d-8980-49ac6db512b5"
                }
            ]
        },
        {
            "id": "92efd67b-48f0-4dd4-8fd9-cffdc17f07ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f5446b1-e870-4bda-bf93-ae3545bc5bf4",
            "compositeImage": {
                "id": "b42f300f-6cdc-4db0-8cbe-861252695517",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92efd67b-48f0-4dd4-8fd9-cffdc17f07ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b93fdd9-fbe5-49cc-abbe-1fa4f73e2fbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92efd67b-48f0-4dd4-8fd9-cffdc17f07ae",
                    "LayerId": "f6be1f5a-0057-493d-8980-49ac6db512b5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f6be1f5a-0057-493d-8980-49ac6db512b5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f5446b1-e870-4bda-bf93-ae3545bc5bf4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 0
}