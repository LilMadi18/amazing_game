{
    "id": "3624aea0-d6b0-49ac-9aa5-84163c35810f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0d348701-93f2-4415-bc03-b1046300fea1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3624aea0-d6b0-49ac-9aa5-84163c35810f",
            "compositeImage": {
                "id": "11ef2574-3e01-481a-bb35-4ec8cf3ea3e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d348701-93f2-4415-bc03-b1046300fea1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7206f38e-1fc6-439f-afe3-be791f3d9894",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d348701-93f2-4415-bc03-b1046300fea1",
                    "LayerId": "b69a0fc6-376b-4ace-ad43-c066d6597976"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b69a0fc6-376b-4ace-ad43-c066d6597976",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3624aea0-d6b0-49ac-9aa5-84163c35810f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}